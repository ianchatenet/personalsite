import mdx from '@astrojs/mdx';
import { loadEnv } from "vite";
import sitemap from '@astrojs/sitemap';
import { defineConfig } from 'astro/config';

import { defaultLang, langs } from './src/utils/i18n/i18n';

const { ASTRO_SITE } = loadEnv(process.env.NODE_ENV, process.cwd(), "");

// https://astro.build/config
export default defineConfig({
	site: ASTRO_SITE,
	integrations: [mdx(), sitemap()],
	i18n: {
		defaultLocale: defaultLang,
		locales: langs,
		routing: {
			prefixDefaultLocale: true
		}
	}
});
