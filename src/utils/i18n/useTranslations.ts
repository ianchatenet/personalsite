import { defaultLang, trads, type TraductionKeys, type UnionLangsType } from "./i18n";

export function useTranslations(lang: UnionLangsType) {
  return function t(key: string) {
    const splitedKeys = key.split('.');
    let defaultTrad: TraductionKeys | string = trads[defaultLang];
    let trad: TraductionKeys | string = trads[lang];

    for (let i = 0; i < splitedKeys.length; i++) {
      const splitedKey = splitedKeys[i];

      if (typeof defaultTrad === 'string') {
        return;
      }

      defaultTrad = defaultTrad[splitedKey];
      trad = (trad as TraductionKeys)[splitedKey];
    }

    if (trad === undefined && defaultTrad === undefined) {
      throw new Error(`key: "${key}" is not valid`);
    }

    return trad || defaultTrad;
  }
}