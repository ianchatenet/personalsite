export const fr = {
  header: {
    nav: {
      blog: "Blog",
      projects: 'Projets'
    },
    socials: {
      gitlab: "Aller à la page de profile gitlab de Ian Chatenet"
    }
  },
  footer: {
    copyrights: "Tous droits réservés.",
    socials: {
      gitlab: "Aller à la page de profile gitlab de Ian Chatenet"
    }
  },
  home: {
    title: "Salut !",
    paragraph: `Moi c'est Ian, développeur depuis 2020 je fais surtout du nodejs mais pas que.
    Actuellement je suis développeur web pour une entreprise qui fait de la robotique.
    Bienvenue sur mon site, ici je partage mes découvertes, mes apprentissages et des projets inutiles.`
  },
  blogPost: {
    lastUpdated: "Dernière mise à jour le"
  },
  tagDetail: {
    title: {
      first: "Posts pour le tag"
    }
  }
};