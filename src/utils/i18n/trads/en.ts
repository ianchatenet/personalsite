import { fr } from "./fr";

export const en: typeof fr = {
  header: {
    nav: {
      blog: 'Blog',
      projects: 'Projects'
    },
    socials: {
      gitlab: "Go to Ian Chatenet's gitlab profile"
    }
  },
  footer: {
    copyrights: "All rights reserved.",
    socials: {
      gitlab: "Go to Ian Chatenet's gitlab profile"
    }
  },
  home: {
    title: "Hi !",
    paragraph: `I'm Ian, developer since 2020, I mainly do nodejs but not only that.
    Currently I am a web developer for a company that does robotics.
    Welcome to my site, here I share my discoveries, my learnings and useless projects.`
  },
  blogPost: {
    lastUpdated: "Last updated on"
  },
  tagDetail: {
    title: {
      first: "Posts for tag"
    }
  }
}