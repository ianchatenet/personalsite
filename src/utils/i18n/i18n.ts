import { en } from "./trads/en";
import { fr } from "./trads/fr";

export const langs = ['fr', 'en'] as const;
export type UnionLangsType = typeof langs[number];

export const defaultLang: UnionLangsType = 'fr';

export interface TraductionKeys extends Record<string, string | TraductionKeys> { }

export type TraductionsDict = Record<UnionLangsType, TraductionKeys>

export const trads: TraductionsDict = {
  fr: fr,
  en: en
} as const;

export function getLangFromUrl(url: URL) {
  const [, lang] = url.pathname.split('/');
  if (lang in trads) return lang as keyof typeof trads;
  return defaultLang;
}