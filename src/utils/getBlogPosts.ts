import { getCollection } from "astro:content";

export async function getBlogPosts(currentLocale: string) {
  const posts = await getCollection("blog", (entry) =>
    entry.slug.includes(currentLocale),
  );
  posts.sort((a, b) => a.data.pubDate.valueOf() - b.data.pubDate.valueOf());
  return posts;
}