import { getProjects } from "./getProjects";

export async function getStaticPathProjects(currentLocale: string) {
  const projects = await getProjects(currentLocale);
  return projects.map((project) => {
    const [_lang, ...slug] = project.slug.split(`${currentLocale}/`);
    return {
      params: { slug: slug.join("/") },
      props: project,
    };
  });
}