import { getCollection } from "astro:content";

export async function getProjects(currentLocale: string) {
  const projects = await getCollection("projects", (entry) =>
    entry.slug.includes(currentLocale),
  );
  projects.sort((a, b) => a.data.pubDate.valueOf() - b.data.pubDate.valueOf());
  return projects;
}