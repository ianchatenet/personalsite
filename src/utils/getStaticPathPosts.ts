import { getBlogPosts } from "./getBlogPosts";

export async function getStaticPathPosts(currentLocale: string) {
  const posts = await getBlogPosts(currentLocale);
  return posts.map((post) => {
    const [_lang, ...slug] = post.slug.split(`${currentLocale}/`);
    return {
      params: { slug: slug.join("/") },
      props: post,
    };
  });
}