import { getProjects } from "./getProjects";
import { getBlogPosts } from "./getBlogPosts";
import type { UnionLangsType } from "./i18n/i18n";
import { BLOG_TAGS, PROJECTS_TAGS } from "../consts";

export async function getStaticPathTag(lang: UnionLangsType, content: "blog" | "projects") {
  if (content === "blog") {
    return getForBlog(lang);
  }

  if (content === "projects") {
    return getForProjects(lang)
  }
}

async function getForBlog(lang: UnionLangsType) {
  const posts = await getBlogPosts(lang);
  return BLOG_TAGS.map((tag) => {
    const filteredPosts = posts.filter((post) => post.data.tags.includes(tag));
    return {
      params: { tag: tag },
      props: {
        posts: filteredPosts,
      },
    };
  });
}

async function getForProjects(lang: UnionLangsType) {
  const projects = await getProjects(lang);
  return PROJECTS_TAGS.map((tag) => {
    const filteredProjects = projects.filter((project) => project.data.tags.includes(tag));
    return {
      params: { tag: tag },
      props: {
        projects: filteredProjects,
      },
    };
  });
}