// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = 'Chatenet Ian';
export const SITE_DESCRIPTION = 'Welcome to my website!';
export const BLOG_TAGS = [
  'rex',
  'dev',
  'monitoring',
];

export const PROJECTS_TAGS = [
  'projects'
];