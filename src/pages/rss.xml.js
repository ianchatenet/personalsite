import rss from '@astrojs/rss';
import { trads } from '../utils/i18n/i18n';
import { getBlogPosts } from '../utils/getBlogPosts';
import { SITE_TITLE, SITE_DESCRIPTION } from '../consts';

export async function GET(context) {
	const [enPosts, frPosts] = await Promise.all([getEnPosts(), getFrPosts()])
	const posts = enPosts.concat(frPosts);
	return rss({
		title: SITE_TITLE,
		description: SITE_DESCRIPTION,
		site: context.site,
		items: posts,
	});
}

async function getEnPosts() {
	const en = trads.en;
	const posts = await getBlogPosts(en);
	return posts.map((post) => ({
		title: post.title,
		pubDate: post.pubDate,
		description: post.description,
		link: `${en}/blog/${post.slug}/`,
	}))
}

async function getFrPosts() {
	const fr = trads.fr;
	const posts = await getBlogPosts(fr);
	return posts.map((post) => ({
		title: post.title,
		pubDate: post.pubDate,
		description: post.description,
		link: `${fr}/blog/${post.slug}/`,
	}))
}