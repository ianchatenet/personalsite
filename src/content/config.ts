import {defineCollection, z} from 'astro:content';
import {BLOG_TAGS, PROJECTS_TAGS} from '../consts';

const blog = defineCollection({
	type: 'content',
	// Type-check frontmatter using a schema
	schema: z.object({
		title: z.string(),
		description: z.string(),
		// Transform string to Date object
		pubDate: z.coerce.date(),
		updatedDate: z.coerce.date().optional(),
		tags: z.array(z.string()).refine(tags => tags.some(tag => BLOG_TAGS.includes(tag))),
	}),
});

const projects = defineCollection({
	type: 'content',
	// Type-check frontmatter using a schema
	schema: z.object({
		title: z.string(),
		description: z.string(),
		// Transform string to Date object
		pubDate: z.coerce.date(),
		updatedDate: z.coerce.date().optional(),
		tags: z.array(z.string()).refine(tags => tags.every(tag => PROJECTS_TAGS.includes(tag))),
	}),
});

export const collections = { 
	blog: blog,
	projects: projects
 };
