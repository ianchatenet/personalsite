---
title: 'Mon année 2024'
description: 'Mes découvertes et apprentissage de 2024'
pubDate: 'Jan 17 2025'
tags: ['rex']
---

J'ai essayé [Astro](https://astro.build/), le framework nous laisse le choix de la "librairie front" que
l'on veut utiliser Vue, React, Solid, Svelte ...
Il y a également la possibilité de gérer son contenu avec des fichiers
markdown, et c'est d'ailleurs ce que je fais pour ce blog.
Avec ce blog, je me suis amusé à déployer sur un VPS depuis
la pipeline [gitlab](https://docs.gitlab.com/ee/ci/pipelines/) du projet en utilisant [rsync](https://fr.wikipedia.org/wiki/Rsync).

Cette année j'ai aussi découvert [Grafana](https://grafana.com/), Loki, Prometheus, Promtail, je fais
tourner tous ces services sur mon VPS pour avoir un affichage des logs du serveur
web, des metrics des conteneurs Docker qui tournent sur la machine et les métriques
du VPS.

Sur ce VPS, je fais également tourner un [serveur Minecraft](https://docker.readthedocs.io/en/latest/) dans un conteneur Docker
j'ai aussi fait un petit bot Discord en Rust qui permet à n'importe qui sur
Le serveur Discord de se whitelist au serveur Minecraft. J'ai aussi setup
[mc-monitor](https://github.com/itzg/mc-monitor) qui permet de remonter dans grafana
des metrics du serveur minecraft.