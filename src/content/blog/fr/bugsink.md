---
title: 'Bugsynk'
description: "Une app d'error tracking self hostable"
pubDate: 'Feb 24 2025'
tags: ['dev', 'monitoring']
---

## D'abord Sentry

Je me suis tout d'abord essayé à tester le lancement de [Sentry](https://sentry.io/welcome/) sur ma machine, pour finir avec mon PC qui redémarre en boucle à cause d'un manque de RAM.

Le moyen le plus simple de démarrer Sentry est de suivre cette documentation [self-hosted Sentry](https://develop.sentry.dev/self-hosted/).
On se retrouve avec un enchaînement monstrueux de scripts shell et Python qui pull des images Docker et démarrent plein, TROP de conteneurs pour les besoins que j'ai. Sur cette même documentation, on peut voir les recommandations matérielles de Sentry, et il faut **16GB de RAM** pour un TRAQUEUR D'ERREURS.

## Léger et simple

Le créateur [klaas van schelven](https://github.com/vanschelven) a eu l'idée de [Bugsink](https://www.bugsink.com/), après avoir auto-héberger une instance Sentry.

Bugsink se lance en 10 minutes, dans un conteneur qui ne me prend que ~570 MiB de mémoire.

Bugsink propose toutes les fonctionnalités dont je peux avoir besoin:
- gestion d'équipe
- gestion de projet, reliée à une équipe
- gestion d'utilisateur, reliée à une équipe
- envoi de mail quand une erreur apparaît
- stacktrace

#### Page détail d'une erreur

![Screenshot de la page de détail d'un erreur dans Bugsink](/blog/bugsink/error-detail-page.png)


Vous pouvez cloner ce dépôt [here](https://gitlab.com/veilles/bugsynk) pour tester toutes les fonctionalités de Bugsink facilement.


