---
title: "C'est quoi un Software Bill Of Materials ?"
description: "Qu'es ce qu'un Software Bill Of Material et á quoi sa sert."
pubDate: 'Mar 01 2025'
tags: ['dev', 'security']
---

## D'ou sa vient ?

Comme son nom le sous-entend, SBOM vient de BOM ou Bill of Materials. C'est une
liste de tout ce qui compose un produit et comment le produit a été construit.
En gros, un SBOM c'est la liste de tous les composants d'un logiciel qui lui 
permettent de fonctionner (libraries, packages, drivers). Pour rentrer plus dans
le détail, c'est en réalité un format d'échange de données, ce n'est pas destiné
à être lu par un humain.  Il existe plusieurs formats de SBOM, les plus utilisés
sont [SPDX](https://spdx.dev/) (Linux Foundation) et [CycloneDX](https://cyclonedx.org/)
(OWASP).

## Pourquoi faire ?

Tout d'abord cela peut permettre de visualiser les dépendances de son logiciel et
possiblement faire le tri. Cela permet aussi de tracker les potentiels vulnérabilités
d'un système.
En Europe, cela va devenir une obligation en décembre 2027 d'être en mesure de
fournir un SBOM pour tout logiciel vendu dans l'Union européenne. Plus d'info ici
[SBOM-europe](https://sbomeurope.eu/),
[Cyber Resilience Act](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32024R2847)

## Comment ?

Je vais utiliser [Trivy](https://trivy.dev/latest/) pour générer un SBOM et scanner une image PostgreSQL 17.4.

Générer un SBOM.

Ici, nous générons un SBOM au format `CycloneDX` et nous générons également un
[Vulnerability Exploitability eXchange (VEX)](https://cyclonedx.org/capabilities/vex/)
grâce au paramètre `--scanners`.

```bash
trivy image --scanners vuln --format cyclonedx --output result.json postgres:17.4
```

Nous nous retrouvons donc avec un JSON de 29 000 lignes, ce qui n'est pas très pratique.

Pour rendre notre SBOM lisible par un humain, nous pouvons utiliser
[Sunshine](https://github.com/CycloneDX/Sunshine/), un outil qui permet de
visualiser les SBOM au format `CycloneDX`. Sunshine va générer un fichier HTML
contenant un diagramme de dépendance ainsi qu'un tableau listant tous les
composants de notre logiciel.

Comme nous avons également créé un VEX, Sunshine nous affiche également les
vulnérabilités de chaque composant.

```bash
python3 sunshine.py -i result.json -o sbom.html
```

Exemple du diagrame générer.

![Screenshot du diagrame générer par sunshine](/blog/sbom/sbom-diagram.png)

Exemple du tableau générer.
![Screenshot du tableau générer par sunshine](/blog/sbom/sbom-table.png)