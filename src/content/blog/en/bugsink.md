---
title: 'Bugsynk'
description: 'Error tracking app self hostable'
pubDate: 'Feb 24 2025'
tags: ['dev', 'monitoring']
---

## First, Sentry

I also tried to test launching [Sentry](https://sentry.io/welcome/) on my machine, only to end up with my PC rebooting in a loop due to a lack of RAM.

The simplest way to start Sentry is to follow this documentation [self-hosted Sentry](https://develop.sentry.dev/self-hosted/).
You end up with a monstrous sequence of shell and Python scripts that pull Docker images and start way too many containers for my needs. In the same documentation, you can see Sentry's hardware recommendations, and it requires **16 GB of RAM** for an ERROR TRACKER.

## Lightweight and Simple

The creator [klaas van schelven](https://github.com/vanschelven) came up with the idea of Bugsink, after self-hosting a Sentry instance.

Bugsink launches in 10 minutes, in a container that only takes up ~570 MiB of memory. Bugsink offers all the features I need:

- team management
- project management, linked to a team
- user management, linked to a team
- email notifications when an error occurs
- stacktrace

#### Error Details Page

![Screenshot of the error details page in Bugsink](/blog/bugsink/error-detail-page.png)

You can clone this repo [here](https://gitlab.com/veilles/bugsynk) to easily test all of Bugsink's features.