---
title: 'Mon année 2024'
description: 'Mes découvertes et apprentissage de 2024'
pubDate: 'Jan 17 2025'
tags: ['rex']
---

I tried [Astro](https://astro.build/), a framework that lets us choose the "front-end library" we want to use: Vue, React, Solid, Svelte, etc.
It also provides the option to manage content with markdown files, which is exactly what I use for this blog.
With this blog, I had fun deploying it to a VPS using the project's [gitlab](https://docs.gitlab.com/ee/ci/pipelines/) pipeline with [rsync](https://fr.wikipedia.org/wiki/Rsync).

This year, I also discovered [Grafana](https://grafana.com/), Loki, Prometheus, and Promtail. I run all these services on my VPS to display web server logs, metrics from the Docker containers running on the machine, and VPS metrics.

On this VPS, I also run a [Minecraft server](https://docker.readthedocs.io/en/latest/) in a Docker container. Additionally, I created a small Discord bot in Rust that allows anyone on the Discord server to whitelist themselves on the Minecraft server. I also set up [mc-monitor](https://github.com/itzg/mc-monitor), which helps to bring Minecraft server metrics into Grafana.